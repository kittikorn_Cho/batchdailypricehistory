﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BataonupdateRund.Db
{
    internal class ConnectDb
    {
        public static MySqlConnection GetConnection_aon()
        {
            MySqlConnection con;
            try
            {
                con = new MySqlConnection();
                con.ConnectionString = connectionString_Aondb();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return con;
        }
        public static string connectionString_Aondb()
        {
            string connection = "server=127.0.0.1; port=3306; database=aondb; user=root; password=P@ssw0rd; Persist Security Info=False; Connect Timeout=300";
            //string connection = "server=192.168.100.254;Uid=sa;PASSWORD=P@ssw0rd;database=WarpSystem;Max Pool Size=400;Connect Timeout=600";//thelord
            //string connection = "server=10.0.0.7;Uid=sa;PASSWORD=admin123;database=WarpUser;Max Pool Size=400;Connect Timeout=600"; //caviar
            //string connection = "server=DESKTOP-UC6A9S0;Uid=sa;PASSWORD=12345;database=WarpUser;Max Pool Size=400;Connect Timeout=600"; //local
            //string connection = "server=192.168.1.152;Uid=sa;PASSWORD=admin123;database=WarpUser;Max Pool Size=400;Connect Timeout=600";//maria
            return connection;
        }
    }
}
