﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BataonupdateRund.Models
{
    internal class Dailypricehistory
    {
        public int Id { get; set; }
        public int CalendarDayId { get; set; }
        public int AngelTypeId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Round { get; set; }
    }
}
