﻿using BataonupdateRund.Db;
using BataonupdateRund.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BataonupdateRund.Services
{
    internal class CarlendarDayService
    {
        public static CarlendarDay Get()
        {
            MySqlConnection con = ConnectDb.GetConnection_aon();
            CarlendarDay calendarDay = new CarlendarDay();
            try
            {
                StringBuilder sql = new StringBuilder();
                con.Open();
                sql.Clear();
                sql.Append("select * from calendarDays where IsDate = true");
                MySqlCommand cmd = new MySqlCommand(sql.ToString(), con);
                MySqlDataReader read = cmd.ExecuteReader();
                DataTable data = new DataTable();
                data.Load(read);
                read.Close();
                if (data.Rows.Count > 0)
                {
                    calendarDay.Id = Convert.ToInt32(data.Rows[0]["Id"]);
                    calendarDay.ToDay = Convert.ToDateTime(data.Rows[0]["ToDay"]);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();
            }
            return calendarDay;
        }
    }
}
