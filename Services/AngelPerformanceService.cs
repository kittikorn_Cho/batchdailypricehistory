﻿using BataonupdateRund.Db;
using BataonupdateRund.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BataonupdateRund.Services
{
    internal class AngelPerformanceService
    {
        public static List<AngelPerformance> Get()
        {
            MySqlConnection con = ConnectDb.GetConnection_aon();
            List<AngelPerformance> angelPerformances = new List<AngelPerformance>();
            try
            {
                con.Open();
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT *FROM aondb.angelperformances LEFT JOIN aondb.calendardays AS C ON aondb.angelperformances.CarlendarDayId = C.Id left join aondb.angels AS A on aondb.angelperformances.AngelId = A.Id left join aondb.agencytypes AS AY on A.AngelTypeId = AY.Id where C.IsDate = 1");
                MySqlCommand cmd = new MySqlCommand(sql.ToString(), con);
                MySqlDataReader reader = cmd.ExecuteReader();
                for (int i = 0; reader.Read(); i++)
                {
                    AngelPerformance angelPerformance = new AngelPerformance();
                    angelPerformance.Id = reader.GetInt32("Id");
                    angelPerformance.CalendarDayId = reader.GetInt32("CarlendarDayId");
                    angelPerformance.AngelId = reader.GetInt32("AngelId");
                    angelPerformance.TotalRound = reader.GetDecimal("TotalRound");
                    angelPerformance.AngelTypeId = reader.GetInt32("AngelTypeId");



                    angelPerformances.Add(angelPerformance);
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            //Console.WriteLine(members);
            return angelPerformances;

        }
        public static bool Update(AngelPerformance angelPerformance)
        {
            MySqlConnection con = ConnectDb.GetConnection_aon();
            bool result = false;
            try
            {
                StringBuilder sql = new StringBuilder();
                con.Open();
                sql.Clear();
                sql.Append("update dailypricehistorys set Round = @TotalRound where CalendarDayId = @CalendarDayId and AngelTypeId = @AngelTypeId");
                MySqlCommand cmd = new MySqlCommand(sql.ToString(), con);
                cmd.Parameters.AddWithValue("@CalendarDayId", angelPerformance.CalendarDayId);
                cmd.Parameters.AddWithValue("@AngelTypeId", angelPerformance.AngelTypeId);
                cmd.Parameters.AddWithValue("@TotalRound", angelPerformance.RoundToatal);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();
            }
            return result;
        }
    }
}
