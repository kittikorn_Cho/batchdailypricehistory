﻿using BataonupdateRund.Db;
using BataonupdateRund.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace BataonupdateRund.Services
{
    internal class DailypricehistoryService
    {
        public static Dailypricehistory GetRound(AngelPerformance angelPerformance)
        {
            MySqlConnection con = ConnectDb.GetConnection_aon();
            Dailypricehistory dailypricehistory = new Dailypricehistory();
            try
            {
                StringBuilder sql = new StringBuilder();
                con.Open();
                sql.Clear();
                sql.Append("select * from dailypricehistorys where CalendarDayId = @CalendarDayId and AngelTypeId = @AngelTypeId ");
                MySqlCommand cmd = new MySqlCommand(sql.ToString(), con);
                cmd.Parameters.AddWithValue("@CalendarDayId", angelPerformance.CalendarDayId);
                cmd.Parameters.AddWithValue("@AngelTypeId", angelPerformance.AngelTypeId);
                MySqlDataReader read = cmd.ExecuteReader();
                if (read.Read()) 
                {
                    dailypricehistory.Round = read.GetDecimal("Round");
                }

                read.Close();
            }
            catch (Exception ex)
            {
                // Handle or log the exception here.
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return dailypricehistory;
        }
    }
}
