﻿
using BataonupdateRund.Models;
using BataonupdateRund.Services;
using System.Diagnostics.Metrics;

namespace AonConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            bool result = InsertBatch();
            if (result)
            {
                Console.WriteLine("Insert Batch Complete");
            }
            else
            {
                Console.WriteLine("Insert Batch Error");
            }

        }

        public static bool InsertBatch()
        {
            bool result = false;
            try
            {
                Console.WriteLine("Insert Batch Waiting...");
                List<AngelPerformance> angelPerformances = AngelPerformanceService.Get();

                foreach (var angelPerformance in angelPerformances)
                {

                    //Console.WriteLine($"ID: {angelPerformance.Id}, TotalRound: {angelPerformance.TotalRound}, AngelId:{angelPerformance.AngelId}, CarlendarDayId:{angelPerformance.CalendarDayId}");

                    //Console.WriteLine($"llllllllll===  {angelPerformance.AngelId}");
                    //Console.WriteLine($"aaaaaaaaa===  {angelPerformance.AngelTypeId}");
                    Dailypricehistory dailypricehistory = DailypricehistoryService.GetRound(angelPerformance);

                    //Console.WriteLine($"Round: {dailypricehistory.Round}");
                    if (angelPerformance.Id != null) 
                    {
                        angelPerformance.RoundToatal = angelPerformance.TotalRound += dailypricehistory.Round;
                        angelPerformance.Id = angelPerformance.Id;
                        angelPerformance.TotalRound = angelPerformance.TotalRound;
                        result = AngelPerformanceService.Update(angelPerformance);
                    }
                  
                    


                    
                }

                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Insert Batch Error: " + ex.Message);
            }
            return result;
        }


    }
}